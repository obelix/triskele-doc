BUILD_DIR=$(shell cd ../.. ; pwd)/build
SRC_DIR=$(shell pwd)

LATEX_BUILD_DIR=$(BUILD_DIR)/latex

$(LATEX_BUILD_DIR)/%.pdf: %.tex base
	cd $(LATEX_BUILD_DIR); pdflatex $<

########################################
all: base compile
base: init

########################################
compile: cut sort build merge ap zip
view: vcut vsort vbuild vmerge vap vzip

cut:	$(LATEX_BUILD_DIR)/cut.pdf
sort:	$(LATEX_BUILD_DIR)/sort.pdf
build:	$(LATEX_BUILD_DIR)/build.pdf
merge:	$(LATEX_BUILD_DIR)/merge.pdf
zip:	$(LATEX_BUILD_DIR)/zip.pdf
ap:	$(LATEX_BUILD_DIR)/ap.pdf

vcut:	$(LATEX_BUILD_DIR)/cut.pdf
	okular $<
vsort:	$(LATEX_BUILD_DIR)/sort.pdf
	okular $<
vbuild:	$(LATEX_BUILD_DIR)/build.pdf
	okular $<
vmerge:	$(LATEX_BUILD_DIR)/merge.pdf
	okular $<
vzip:	$(LATEX_BUILD_DIR)/zip.pdf
	okular $<
vap:	$(LATEX_BUILD_DIR)/ap.pdf
	okular $<

$(LATEX_BUILD_DIR)/ap.pdf: ap.tex commonBuild/*.tex
$(LATEX_BUILD_DIR)/build.pdf: build.tex commonBuild/*.tex
$(LATEX_BUILD_DIR)/cut.pdf: cut.tex commonBuild/*.tex
$(LATEX_BUILD_DIR)/merge.pdf: merge.tex commonBuild/*.tex
$(LATEX_BUILD_DIR)/sort.pdf: sort.tex commonBuild/*.tex

########################################
init:
	mkdir -p $(LATEX_BUILD_DIR)
	for i in *.tex commonBuild/; do ln -fs $(SRC_DIR)/$$i $(LATEX_BUILD_DIR)/; done
	ln -fs $(SRC_DIR)/../images $(BUILD_DIR)/
clean:
	find . -type f '(' -name '#*' -o -name '*~' ')' -print -exec rm -f '{}' \;
	test ! -d $(BUILD_DIR) || ( \
		cd $(LATEX_BUILD_DIR) ; \
		find . -type f '(' -name '#*' -o -name '*~' -o -name core ')' -print -exec rm -f '{}' \; \
		rm -f *.aux *.log *.bbl *.blg *.idx *.nlo *.tns ; \
		rm -f *-blx.bib *.dvi *.nav *.out *.run.xml *.snm *.toc *.vrb *.lo[ftl] ; \
	)
wipePdf:
	test ! -d $(LATEX_BUILD_DIR) || \
		for i in cut sort build merge ap zip; do rm -fr $(LATEX_BUILD_DIR)/$$i.pdf; done
wipe: clean wipePdf
	test ! -d $(LATEX_BUILD_DIR) || \
		for i in *.tex commonBuild; do rm -f $(LATEX_BUILD_DIR)/$$i; done
	test ! -d $(BUILD_DIR) || rm -f $(BUILD_DIR)/images
	- test ! -d $(BUILD_DIR) || rmdir $(LATEX_BUILD_DIR) $(BUILD_DIR)
