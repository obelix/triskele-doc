#!/bin/bash

# the alias map the host directory "/data/images" to docker directory "/images"
# form arles images compose by 3 bands [0, 1, 2] we only consider the band "0"
# the new band 3 is created, and we copy this new band as output
prg -i /images/arles.tif -o /images/result.tif --sobelBands 0 --copyBands 3

# or
prg -i /images/arles.tif -o /images/result.tif -s 0 -c 3

# view le greyscale image result
qgis /data/images/result.tif
