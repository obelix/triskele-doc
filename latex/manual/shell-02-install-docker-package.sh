#!/bin/bash
# Install docker package
sudo apt update
sudo apt install apt-transport-https ca-certificates curl gnupg2 \
     software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg |
    sudo apt-key add -
sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/debian bullseye stable"
sudo apt update
sudo apt install docker-ce
sudo systemctl status docker
sudo usermod -a -G docker ${USER}
if ! grep -qw docker 2>/dev/null <<<$(id); then
    echo "*** You need to log out now! ***" ;
fi
