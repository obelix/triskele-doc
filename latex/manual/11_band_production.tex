\section{Options for bands production}
\label{sec:band-production}

This section deals with choosing the number and nature of the bands to produce in the output image.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{-c arg, {\doubleDash}copyBands arg}
Copy the selected bands. Please note that those bands can be either:

\begin{itemize}
\item bands from input image (numbers starting from 0)
\item NDVI or Sobel bands just produced. In that case you have to known the corresponding number.
\end{itemize}

Please see some examples in section \ref{sec:examples}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{-n arg, {\doubleDash}ndviBands arg}

Produces NDVI bands from 2 input bands. We remind that the bands are numbered starting from 0. We remind that this option needs to be used with option {\doubleDash}copyBands in order to keep this band.

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
prg /images/arles.tif /images/ndvi.tif -n 0,1 -c 3
\end{lstlisting}

\begin{center}
\begin{tabular}{c c c}
    source & ~ & NDVI (0,1) \\
    \includegraphics[width=3cm]{../images/prodBand/arles-mini.png}\includegraphics[width=3cm]{../images/prodBand/arles-crop.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-mini-NDVI.png}\includegraphics[width=3cm]{../images/prodBand/arles-crop-NDVI.png} \\
    & & 0.014 s \\
\end{tabular}
\end{center}

\subsubsection{Computation steps}
The computation is made in 2 stages:

\begin{itemize}
\item first band is read and stored
\item second band is read and the computation is such as:
\newline
\begin{tabular}{ll}
    $NDVI = [(A-B)/(A+B)+1]*MaxVal/2$   & where $A+B\neq0$ \\
    $NDVI = 0$                          & where $A+B=0$ (i.e. $A=0$ and $B=0$ simultaneously) \\
\end{tabular}
\end{itemize}

Note that as the ratio $(A-B)/(A+B)$ would be in interval $[-1;1]$, we added an extra normalization to make it fit in in the interval $[0;MaxVal]$ where $MaxVal$ is the maximum possible value of the initial images. Also note that if the 2 input bands are inverted, then the resulting band will have opposite values.

\subsubsection{Precision}
The resulting band is set at the same precision than the input bands (ex. 8 bits or 16 bits).

\subsubsection{Complexity}
The complexity is linear with the number of pixels (each pixel from the input bands is read once).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{ -p arg, {\doubleDash}PantexBands arg}

Produces Pantex bands of the input bands. TThis command produces Pantex band of the input image according to the method introduced in \cite{4660321}. This feature relies on gray-level co-occurrence matrix (GLCM).

First, all the pixels of the image are sorted. 15\% of the pixels with lowest values are removed and substituted with the smallest remaining pixel value. Also 15\% of the pixels with highest values are removed and substituted with the largest remaining pixel value. Then, dynamic range of the pixels is scaled to the interval of $[0,127]$.


A $w*w$ sliding window moves on the image. Inside each sliding window we consider 10 different offsets corresponding to 10 different combinations of distances and angles (see \ref{fig:pantexVector}). A co-occurrence matrix is computed separately for each offset.


Co-occurrence of an image is a matrix that has a row and a column for each gray level of the image. (See \ref{fig:pantexGLCM}). The position (i,j) in co-occurrence matrix shows the number of times that gray level j occurs at a specific offset of gray level i in original image.

For Pantex 10 co-occurrence matrices are calculated based on 10 offsets for each sliding window. One of these offsets is shown in \ref{fig:pantexSliding} corresponding to red square in \ref{fig:pantexVector}. Each co-occurrence matrix is normalized by dividing by the sum of its elements. So the sum of all elements in final co-occurrence matrix is equal to 1.

Contrast of each gray-level co-occurrence matrix (GLCM) is defined as 

$contrast=\sum\limits_{i=0}^{127} \sum\limits_{j=0}^{127} (i-j)^2 . GLCM_{i,j} $.

Pantex is the minimum of 10 contrasts that are calculated for $10$ co-occurrence matrices.

\newcommand{\deltaPairs}{1/-2,  1/-1,  2/-1, 1/0, 2/0, 0/1, 1/1, 2/1, 0/2, 1/2}

\newcommand{\drawCell}[3]{\draw [fill=#3] (#1,#2) rectangle (#1+1, #2+1)}

\begin{figure}[htbp]
  \centering
  \begin{minipage}[b]{0.2\textwidth}    \centering
    \begin{tikzpicture}[scale=.25]
      \draw [help lines] (0,0) grid (5, 5);
      \draw [fill=gray] (2,2) rectangle (3, 3);
      \foreach \dx/\dy in \deltaPairs {
        \draw [fill=green!30] (2+\dx,2-\dy) rectangle (3+\dx,3-\dy);
      }
      \draw [red] (3,4) rectangle (3+1, 4+1);
    \end{tikzpicture}
    \caption{10 vectors}
    \label{fig:pantexVector}
  \end{minipage}
  \begin{minipage}[b]{0.4\textwidth}
    \centering
    \begin{tikzpicture}[scale=.7]
      \begin{axis} [no markers,xmin=0,xmax=5,ymin=0,ymax=5,xticklabel=\empty,yticklabel=\empty,xlabel=$centroide gray level$,ylabel=$neighbour gray level$]
  \end{axis}
  \draw [fill=gray] (2,0) rectangle (2+.5, .5);
  \draw [fill=green!30] (0,3) rectangle (.5, 3+.5);
  \node [draw] at (2+.25,3+.25) {\large +1};
    \end{tikzpicture}
    \caption{GLCM}
    \label{fig:pantexGLCM}
  \end{minipage}
  \begin{minipage}[b]{0.3\textwidth}
    \centering
    \begin{tikzpicture}[scale=.25]
      \draw [|<->|] (0,11.5) -- node[above] {window pantex width} (11,11.5);

      \draw [help lines] (0,0) grid (11, 11);
      \drawCell{5}{5}{gray};
      \drawCell{5+1}{5+2}{green!30};
      \draw [ultra thick, green] (1,2) rectangle (11,11);
      \draw [ultra thick, gray] (0,0) rectangle (10,9);
      \draw [ultra thick, arrows={-latex'}] (.5,.5) -> (1.5,2.5);
    \end{tikzpicture}
    \caption{Sliding window}
    \label{fig:pantexSliding}
  \end{minipage}
\end{figure}

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
gdal_calc.py -R arles.tif --R_band=1 -G arles.tif --G_band=2 -B arles.tif --B_band=3 --outfile=arlesGS.tif '--calc=R*0.2989+G*0.5870+B*0.1140'

prg /images/arlesGS.tif /images/pantex5.tif -p 0 -c 1 --pantexWindowSide 5

prg /images/arlesGS.tif /images/pantex63.tif -p 0 -c 1 --pantexWindowSide 63

prg /images/arlesGS.tif /images/pantex127.tif -p 0 -c 1 --pantexWindowSide 127

prg /images/arlesGS.tif /images/pantex35.tif -p 0 -c 1

\end{lstlisting}

\begin{center}
\begin{tabular}{c c c c c c c}
    source & ~ & Pantex (5x5) & ~ & Pantex (63x63) & ~ & Pantex (127x127) \\
    \includegraphics[width=3cm]{../images/prodBand/arles-mini-GS.png} & & 
    \includegraphics[width=3cm]{../images/Pantex/arles-mini-Pantex5.png} & & 
    \includegraphics[width=3cm]{../images/Pantex/arles-mini-Pantex63.png} & & 
    \includegraphics[width=3cm]{../images/Pantex/arles-mini-Pantex127.png} \\
    \includegraphics[width=3cm]{../images/prodBand/arles-crop-GS.png} & & 
    \includegraphics[width=3cm]{../images/Pantex/arles-crop-Pantex5.png} & & 
    \includegraphics[width=3cm]{../images/Pantex/arles-crop-Pantex63.png} & & 
    \includegraphics[width=3cm]{../images/Pantex/arles-crop-Pantex127.png} \\
    & & 0.91 s
    & & 7.18 s
    & & 13.08 s \\
\end{tabular}
\end{center}

\subsubsection{Computation steps}

The complexity of the original algorithm depend of $g$ number of neighbours, $w$ the side of the sliding window, $n$ the number of pixels.

$O(g . w^2 . n)$

Because the Pantex index could be customized by the size of the sliding windows our approach is to reduce the impact of $w$.

We first of all define a pre-computed matrix $S$ for square delta gray scale values.

$S_{i,j} = (i-j)^2$

We delay the normalization of GCLM. We call $M$ the true occurrence matrix.

In that case, we rewrite the definition as 

$pantex =
\min_{sw} \sum\limits_{i=0}^{127} \sum\limits_{j=0}^{127} (i-j)^2 . GLCM_{i,j} =
\min_{sw} \frac{ \sum\limits_{i=0}^{127} \sum\limits_{j=0}^{127} S_ {i,j} . M_{i,j}}{card(slidingWindow)} =
\min_{sw} \frac{C}{card(slidingWindow)}
$

Let call $C$ the sum of co-occurrence by the square of delta gray level according a class neighbour.
The difference between C of to consecutive pixel depend on forgotten pairs of neighbour in a side and new pairs of neighbours in the opposite side.

As shown in figure \ref{fig:pantexLinear} to find the Pantex index of the next pixel, we remove impact of the old side (orange one) and add impact of the new side (blue one).

\begin{figure}[htbp]
  \centering
  \begin{minipage}[b]{.25\textwidth}
    \centering
    \begin{tikzpicture}[scale=.35]
      \draw [fill=orange!30] (1,11) rectangle (1+10, 11+1);
      \draw [fill=orange!30] (0,9) rectangle (0+10, 9+1);
      
      \draw [fill=blue!30] (1,2) rectangle (1+10, 2+1);
      \draw [fill=blue!30] (0,0) rectangle (0+10, 0+1);

      \draw [help lines] (0,0) grid (11, 11);

      \drawCell{5}{6}{gray!50};
      \drawCell{5+1}{5+3}{red!30};

      \drawCell{5}{5}{gray};
      \drawCell{5+1}{5+2}{green!30};

      \draw [ultra thick, red] (1.1,3.1) rectangle (11.1,12.1);
      \draw [ultra thick, green] (1,2) rectangle (11,11);
      \draw [ultra thick, gray] (0,0) rectangle (10,9);
      \draw [ultra thick, arrows={-latex'}] (.5,.5) -> (1.5,2.5);
    \end{tikzpicture}
    \caption{Pantex linear}
    \label{fig:pantexLinear}
  \end{minipage}
  \begin{minipage}[b]{.7\textwidth}
    \centering
    \begin{tikzpicture}
      \begin{axis} [no markers, xlabel=pantex window side, ylabel=execution time (second), legend style={at={(.2,1)},
	anchor=north}]
        \addplot table [x=pantex, y=1, col sep=comma] {pantexPerf.csv};
        \addlegendentry{1 core};
        \addplot table [x=pantex, y=2, col sep=comma] {pantexPerf.csv};
        \addlegendentry{2 cores};
        \addplot table [x=pantex, y=4, col sep=comma] {pantexPerf.csv};
        \addlegendentry{4 cores};
        \addplot table [x=pantex, y=8, col sep=comma] {pantexPerf.csv};
        \addlegendentry{8 cores};
        \addplot table [x=pantex, y=16, col sep=comma] {pantexPerf.csv};
        \addlegendentry{16 cores};
        \addplot table [x=pantex, y=32, col sep=comma] {pantexPerf.csv};
        \addlegendentry{32 cores};
      \end{axis}
    \end{tikzpicture}
    \caption{Pantex performance}
    \label{fig:pantexPerf}
  \end{minipage}
\end{figure}

The algorithm become (where $i$ and $j$ come from the position of pixels $old$ and $new$) :

$C_{i} = C_{i-1} - \sum\limits_{old} S_ {i,j} +  \sum\limits_{new} S_ {i,j}$

The complexity become linear $O(g . w . n)$


\subsubsection{Precision}

The Pantex index depend on the square of gray scale value ($[0 .. 127]$ in our case).
So it is in $[0 .. 127^2]$

If the input Pixel is a byte, we scale the Pantex by taking the square root. Other else the value is copy as the same type as the input pixels.

\subsubsection{Complexity}

The complexity is linear with the number of pixels and the side of the Pantex window.

We chose to move the sliding window according the y axis to reduce the memory default pages.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{ -s arg, {\doubleDash}sobelBands arg}

Produces Sobel bands, i.e. bands containing the Sobel gradient of the input band.
We remind that this option needs to be used with option {\doubleDash}copyBands in order to keep this band.

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
prg /images/arlesGS.tif /images/sobel.tif -s 0 -c 1

prg /images/arles.tif /images/sobel.tif -n 0,1 -s 3 -c 4

prg /images/arlesGS.tif /images/sobel.tif -p 0 -s 1 -c 2
\end{lstlisting}

\begin{center}
\begin{tabular}{c c c c c}
    source & ~ &  & ~ & Sobel (0) \\
    \includegraphics[width=3cm]{../images/prodBand/arles-mini-GS.png}\includegraphics[width=3cm]{../images/prodBand/arles-crop-GS.png} & &
    & &
    \includegraphics[width=3cm]{../images/prodBand/arles-mini-Sobel.png}\includegraphics[width=3cm]{../images/prodBand/arles-crop-Sobel.png} \\
    & & & & 0.084 s\\
\end{tabular}
\begin{tabular}{c c c c c}
    source & ~ & NDVI (0,1) & ~ & Sobel (NDVI (0,1)) \\
    \includegraphics[width=3cm]{../images/prodBand/arles-mini.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-mini-NDVI.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-mini-SobelN.png} \\
    \includegraphics[width=3cm]{../images/prodBand/arles-crop.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-crop-NDVI.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-crop-SobelN.png} \\
    & & 0.028 s & & 0.081 s\\
\end{tabular}
\begin{tabular}{c c c c c}
    source & ~ & Pantex (35) & ~ & Sobel (Pantex (35)) \\
    \includegraphics[width=3cm]{../images/prodBand/arles-mini-GS.png} & & \includegraphics[width=3cm]{../images/Pantex/arles-mini-Pantex35.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-mini-SobelP.png} \\
    \includegraphics[width=3cm]{../images/prodBand/arles-crop-GS.png} & & \includegraphics[width=3cm]{../images/Pantex/arles-crop-Pantex35.png} & & \includegraphics[width=3cm]{../images/prodBand/arles-crop-SobelP.png} \\
    & & 12.22 s & & 0.22 s\\
\end{tabular}
\end{center}


\subsubsection{Computation steps}
The Sobel band computation consists of the 2 following 3 steps:


\[ 
G_x
= I * 
\begin{bmatrix}
    -1 & 0 & 1 \\
    -2 & 0 & 2 \\
    -1 & 0 & 1 \\
\end{bmatrix}
= I * 
\begin{bmatrix}
    -1 & 0 & 1 \\
\end{bmatrix}
*
\begin{bmatrix}
    1 \\
    2 \\
    1 \\
\end{bmatrix}
\]

\[ 
G_y
= I * 
\begin{bmatrix}
    -1 & -2 & -1 \\
     0 &  0 &  0 \\
     1 &  2 &  1 \\
\end{bmatrix}
= I * 
\begin{bmatrix}
    -1 \\
     0 \\
     1 \\
\end{bmatrix}
*
\begin{bmatrix}
    1 & 2 & 1 \\
\end{bmatrix}
\]


\[ 
G=\sqrt{{G_x}^2+{G_y}^2} \simeq |G_x| + |G_y|
\]

\subsubsection{Precision}
The resulting band is set at the same precision than the input bands (ex. 8 bits or 16 bits).

\subsubsection{Complexity}
The complexity is linear with the number of pixels (each pixel from the input bands is read 4 times).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{{\doubleDash}noCopy}

Do not keep the selected bands (either input image, or NDVI, Sobel or Pantex bands) in the list of bands to produce

\subsection{{\doubleDash}noNdvi}

Do not keep the NDVI band in the list of bands to produce

\subsection{{\doubleDash}noPantex}

Do not keep the Pantex bands in the list of bands to produce

\subsection{{\doubleDash}noSobel}

Do not keep the Sobel bands in the list of bands to produce

