\section{Hierarchical representation of images}
\label{sec:hierarchical}

\subsection{Tree representation}

A hierarchical representation of the content of the images can be made by using morphological trees, here most often inclusion trees.

Those trees are constructed in such a way that the leaves are small regions of pixels with constant value, and the nodes are connected regions created by adding surrounding pixels with very close values.
In that way all possible pixel values are represented up to the value of the root node, which can be either the minimum, the maximum or the median value, according to the type of tree chosen.

Let’s take an example using pixel values between 0 (black) and 255 (white). We will build the tree node by node, from the leaves to the root by browsing all this interval of pixels values. And the order that we choose to browse the interval will define the type of tree we build:

\begin{itemize}
\item If we order the pixel values from 0 (dark) to 255 (white), then the leaves will be local minima, and the tree will be called a ``\textbf{min-tree}''.
\item If we order the pixel values from 255 (white) down to 0 (dark), then the leaves will be local maxima, and the tree will be called a ``\textbf{max-tree}''.
\item If we order the pixel values using the distance to the median value, beginning from the farthest values to the median (either greater or lower) then getting closer and closer to the median, then the leaves will be local extrema (minima and/or maxima), and the tree will be called a ``\textbf{tree of shape}''. We can mention that the name “tree of shape” comes from the fact that it creates the different nodes by mixing in parallel “bright” and “dark” nodes from the median node, therefore focuses more of the shape of the nodes than on their gray-scale.
\item Actually the tree described before is not the official tree-of-shape, but rather an optimized version which is called ``\textbf{median-tree}'', using the median value to compute it faster.
\end{itemize}

Also, note that we can also visualize this tree representation with a “lake model”. First think of the initial image with a 3D representation, where the intensity would be replaced by a third dimension (altitude), so that the image is like a landscape made with mountains (bright pixels) and valleys (dark pixels). Then each node would be a “lake” partially filling the valleys and partially reduced by the mountains, and having a constant altitude (i.e. on the 2D image having a constant intensity). Then the min-tree is a set of all possible lakes from the lowest case to the highest. The max-tree describes the dual situation, that is a set of all possible emerged surface regions from the highest case to the lowest. Finally the tree of shape describes all the possible lakes and/or emerged surface regions from the most extreme (lowest and/or highest) ones to the average ones (see figure \ref{fig:3-trees-principle}, for now look only at column ``Orig. images'').

\subsection{Efficient implementation}

To maintain an efficient implementation, all algorithms integrated into the software have linear or quasi-linear time complexity \cite{merciol:hal-02343814}.
This is a brief presentation of the hidden data structure with compact and linear information that help us to design attribute algorithms.

\subsubsection{Tree and attributes implementation}

First of all, we use the fundamental properties of hierarchical data representation. Consider a gray scale image (left side of Figure \ref{fig:triskele-image}). This image is a matrix of pixels. We can build different types of trees (min-tree, max-tree, \dots) to manage the image. We choose to build a max-tree (right side of figure \ref{fig:triskele-image}).

This tree is a hierarchical representation. All pixels have parent. These parents are called nodes. All nodes (except the root) also have a parent node. This defines an inclusion tree.
Because we choose a max-tree for this example, the nodes are organized from light gray (low level of the tree) to dark gray (high level). All nodes can have properties. We represent them with colored bullets in Figure \ref{fig:triskele-image}.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=.4\linewidth]{../images/triskele-image.pdf}
    \caption{Hierarchical representation of image using a max-tree}
    \label{fig:triskele-image}
\end{figure}

The tree is defined only with an array of parents (see Figure \ref{fig:triskele-array}).
All pixels and nodes are associated with a unique index.
The indexes under ``pixel cardinality'' refer to the pixels from the first row to the last row of the image. Indexes with higher ``cardinality of pixels'', refer to tree nodes. Thanks to this building process, all nodes are sorted according to their level. So the light-gray nodes are on the right and the darker-gray ones are on the left.

\begin{figure}[ht!]
    \centering
    \includegraphics[width=.6\linewidth]{../images/triskele-array.pdf}
    \caption{Tree implemented with array}
    \label{fig:triskele-array}
\end{figure}

Attributes are only defined for nodes. The attributes can be simple scalar (surface, perimeter, gray value) or multivalued (centroid, bounding box), and they are also stored in another array. The position of an attribute in an array gives the position of the associated node. Here are our efficient calculations of new attributes.

\subsection{Attribute profiles}

The main goal of the tree previously build is to create what we call the Attribute Profiles (AP) of the image. The attribute profiles are obtained by pruning the tree (i.e. excluding some leaves). More precisely the different steps are:

\begin{itemize}
\item create a tree from the image, where each node of the tree contains a gray scale value
\item for each node, compute also some extra attributes, such as area or standard deviation
\item prune the tree, according to either gray scale levels or any type of attributes, and do this several times using a series of thresholds
\footnote{For optimization reason, the algorithm does not actually prune the tree, but instead walks through the entire tree from the leaves to the root until it reaches a node above a given threshold, which will then be considered as a new ``leaf'' of the pruned tree}.
We can notice that the tree pruning is a projection
\item for each pruned tree, rebuild the image using only the remaining nodes (filtered image)
\item put all the filtered images together in a stack, which will be called the “attribute profiles”
\end{itemize}

According to the type of tree we consider, here is the result of this process on the tree and on the image (see figure \ref{fig:3-trees-principle}):

\begin{itemize}
\item in the case of min-tree, the tree pruning removes the nodes from its darkest to its brightest, so that the image filtering replaces the darkest areas by merging them into brighter and brighter surrounding zones (corresponding to their parent nodes).
\item in the case of max-tree, the tree pruning removes the nodes from its brightest to its darkest, so that the image filtering replaces the brightest areas by merging them into darker and darker surrounding zones.
\item in the case of tree of shape, the tree pruning removes the nodes from its most extreme (brightest and darkest) to its most average (with median value), so that the image filtering replaces the most extreme (brightest and/or darkest) areas by merging them into less and less extreme surrounding zones.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=10cm]{../images/min-tree-principle.pdf}
\includegraphics[width=10cm]{../images/max-tree-principle.pdf}
\includegraphics[width=6cm]{../images/tree-of-shapes-principle.pdf}
\caption{Principle of a Min-Tree, a Max-Tree and a Tree-of-Shape}
\label{fig:3-trees-principle}
\end{figure}

\subsection{Feature profiles}

The Feature Profiles (FP) is a concept very similar to the Attribute Profiles, except that instead of producing a stack of images containing gray-values (just as the original image), we will produce a similar stack of images but containing a given feature, such as area, moment of inertia, or standard deviation.

Note that they are two independent characteristics used in this process (which could either be the same or be different):

\begin{enumerate}

\item First the characteristics called ``attribute'' used to prune the tree, according to a series of thresholds. For example we can compute for each node the area of the node, then prune the tree according to different thresholds of the area. For a series of N thresholds, this will produce N pruned trees.

\item Then the characteristics called ``feature'' used to reconstruct the corresponding stack of images (also called ``filtered images'').
\begin{itemize}
\item if we choose the gray scale-level, the stack will be called ``Attribute Profile''
\item if we choose any other types (area, standard deviation, ...), the stack will be called ``Feature Profile''
\end{itemize}

\end{enumerate}

Also note in the future sections, we will also talk about ``Haar-like features'' and ``statistical features''. Those features are completely different characteristics, absolutely independent of the attributes or features we are discussing now.

