\section{Options for Differential Attribute Profiles (DAP)}

First note that the following option dapPos will automatically trigger the production of Differential Attribute Profiles (DAP) instead of Attribute Profiles (AP). The Differential Attribute Profiles are simply a series of images that are the difference between 2 different Attribute Profiles.

\subsection{{\doubleDash}dapPos arg}

The DAP images are usually computed between 2 filtered image. However in addition we can also compute the difference between a filtered image and the original image. In that case we have to specify between which filtered image and the original the subtraction will be made.
Actually this situation is equivalent to inserting the original image in the list of filtered image, then make the difference image of all pairs of successive image of the list. So this option will specify in which position the original will be inserted inside the list of filtered images. Note that we might want to insert it more than once. The following table shows all the possible positions. In order to visualize the position of the original in the list of other images, we also add a diagram representation of the list of images using the following notations:


\begin{itemize}
\item o: original image
\item a, b, c, ...: series of filtered images found in the attribute profiles, made with the different thresholds in this series, we assume that the thresholds are ordered from the lowest to the highest
\item A, B, ...: difference between 2 successive images
\end{itemize}

% note : \newline works only with column options p, m or b
\begin{tabular}{|c|m{3cm}|m{8cm}|}
    \hline
    Name & 
    Diagram representation & 
    Compute the difference between: \\
    \hline

    apOrigNoPos &
    \includegraphics[width=4cm]{../images/dap/dap-pos-0.pdf} &
    - all successive filtered images \\
    \hline

    apOrigPosBegin &
    \includegraphics[width=4cm]{../images/dap/dap-pos-1.pdf} &
    - all successive filtered images \newline
    - the original image and the first filtered image (lowest threshold) \\
    \hline

    apOrigPosEnd &
    \includegraphics[width=4cm]{../images/dap/dap-pos-2.pdf} &
    - all successive filtered images \newline
    - the original image and the last filtered image (highest threshold) \\
    \hline

    apOrigPosBoth &
    \includegraphics[width=4cm]{../images/dap/dap-pos-3.pdf} &
    - all successive filtered images \newline
    - the original image and the first filtered image (lowest threshold) \newline
    - the original image and the last filtered image (highest threshold) \\
    \hline

    apOrigPosEverywhere &
    \includegraphics[width=4cm]{../images/dap/dap-pos-4.pdf} &
    - all the filtered image and the original image \\
    \hline
\end{tabular}

\subsection{{\doubleDash}dapWeight}

Multiply the DAP images by the original image. This option is useful to obtain DAP images with a better contrast.

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 -f AP -t Med -a Area --thresholds 10,100,5000 --dapPos apOrigNoPos --auto --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c}
    A & & B & & apOrigNoPos \\
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigNoPos-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigNoPos-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigNoPos-ap-area-10-100-5000.png} \\
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigNoPos-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigNoPos-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigNoPos-ap-area-10-100-5000.png} \\
  \end{tabular}
\end{center}

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 -f AP -t Med -a Area --thresholds 10,100,5000 --dapPos apOrigPosBegin --auto --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c c c c}
    A & & B & & C & & apOrigPosBegin \\
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBegin-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBegin-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBegin-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBegin-ap-area-10-100-5000.png} \\
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBegin-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBegin-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBegin-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBegin-ap-area-10-100-5000.png} \\
  \end{tabular}
\end{center}


The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 -f AP -t Med -a Area --thresholds 10,100,5000 --dapPos apOrigPosEnd --auto --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c c c c}
    A & & B & & C & & apOrigPosEnd \\
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEnd-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEnd-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEnd-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEnd-ap-area-10-100-5000.png} \\
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEnd-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEnd-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEnd-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEnd-ap-area-10-100-5000.png} \\
  \end{tabular}
\end{center}


The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 -f AP -t Med -a Area --thresholds 10,100,5000 --dapPos apOrigPosBoth --auto --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c c c c c c c}
    A & & B & & C & & D & & apOrigPosBoth \\
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBoth-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBoth-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBoth-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBoth-D-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosBoth-ap-area-10-100-5000.png} \\
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBoth-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBoth-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBoth-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBoth-D-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosBoth-ap-area-10-100-5000.png} \\
  \end{tabular}
\end{center}

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 -f AP -t Med -a Area --thresholds 10,100,5000 --dapPos apOrigPosEverywhere --auto --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c c c c}
    A & & B & & C & & apOrigPosEverywhere \\
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEverywhere-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEverywhere-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEverywhere-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-mini-apOrigPosEverywhere-ap-area-10-100-5000.png} \\
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEverywhere-A-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEverywhere-B-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEverywhere-C-ap-area-10-100-5000.png} & & 
    \includegraphics[width=2.4cm]{../images/dap/arles-crop-apOrigPosEverywhere-ap-area-10-100-5000.png} \\
  \end{tabular}
\end{center}

