alias prg="docker run -v /tmp/images:/images \
 -it registry.gitlab.inria.fr/obelix/broceliande/master:latest"

alias cfg="docker run -v /tmp/images/:/images \
 --entrypoint /broceliande/build/broceliandeConfig
 -it registry.gitlab.inria.fr/obelix/broceliande/master:latest"
