alias prg="docker run -v /data/images:/images \
 -it registry.gitlab.inria.fr/obelix/triskele/master:latest"

alias cfg="docker run -v /data/images/:/images \
 --entrypoint /triskele/build/channelConfig \
 -it registry.gitlab.inria.fr/obelix/triskele/master:latest"
