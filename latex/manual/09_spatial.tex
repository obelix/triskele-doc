\section{Options for spatial selection and connectivity (\prgName only)}

This section deals with spatial selection (i.e. cropping image) and pixel connectivity.

Working on a cropped image can be useful to speed up a given process by testing it on a smaller image. Cropping an image means reducing it to a smaller rectangle. Now in order to define the rectangle, we usually provide 4 parameters (left border, top border, width, height). Here it is possible to provide only some of the 4 previous parameters, and the missing parameters will be deduced (see details below).
The approach is that a missing position indicates centered cropping. And that a missing size indicates the maximum occupancy.
Note that if none of the cropping parameters are provided, then the entire image will be taken into account.

As parameters, one can provide a number of pixels or a ration of the maximum size.

\subsection{-x dimImg, {\doubleDash}left dimImg} (=-1)

Defines the left border of the cropped image, in pixels. This values must be in the interval $[0, imageWidth-1]$ or $[0\%, 100\%]$. If not provided, the value will be chosen so that the cropped image will be horizontally centered.

\subsection{-y dimImg, {\doubleDash}top dimImg} (=-1)

Defines the top border of the cropped image, in pixels. This values must be in the interval $[0, imageHeight-1]$ or $[0\%, 100\%]$. If not provided, the value will be chosen so that the cropped image will be vertically centered.

\subsection{-w dimImg, {\doubleDash}width dimImg} (=-1)

Defines the width of the cropped image, in pixels. This values must be in the interval $[0;imageWidth-1]$ or $[0\%, 100\%]$. If not provided, the value will be chosen so that the cropped image fill the right side of the image (all the remaining width after the new left border is used).

\subsection{-h dimImg, {\doubleDash}height dimImg} (=-1)

Defines the height of the cropped image, in pixels. This values must be in the interval $[0;imageHeight-1]$ or $[0\%, 100\%]$. If not provided, the value will be chosen so that the cropped image fill the bottom side of the image (all the remaining height below the new top border is used).

\vspace{1em}
Examples

\begin{tabular}{|c|c|c|c|c|c|}
  \hline
  parameter & -1 & 0 & 100 & 10\% & 100\% \\
  \hline
  -x & centered & left border & 100 pixels from left & $1/10$ from left & nonsense \\
  \hline
  -y & centered & top border & 100 pixels from top & $1/10$ from top & nonsense \\
  \hline
  -w & like 100\% & nonsens & 100 pixels & $1/10$ of width & full width but left border \\
  \hline
  -h & like 100\% & nonsens & 100 pixels & $1/10$ of height & full width but top border \\
  \hline
\end{tabular}

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
prg /images/i.tif /images/crop.tif -c 0-* -x -1 -y 1000 -w "50%"
\end{lstlisting}

\subsection{{\doubleDash}connectivity arg (\prgName only)}

Force to use a given pixel connectivity definition. Pixel connectivity defines which surrounding pixels are considered as neighbors. Here are the different possible definition for a pixel neighborhood (see figure \ref{fig:connectivity}):

\begin{itemize}

\item C4: 4 surrounding pixels : one above, one below, one at the left and one at the right.

\item C6P: 6 surrounding pixels: the 4 C4 pixels plus one at the top-left corner and one the 
bottom-right corner.

\item C6N: 6 surrounding pixels: the 4 C4 pixels plus one at the top-right corner and one the bottom- left corner.

\item C8: 8 surrounding pixels.

\item CT (for time series):
\begin{itemize}
\item no neighbors in the current image (time t)
\item in the previous image (time t-1) and the next image (time t+1) : use same pixel
\end{itemize}

\item CTP (for time series):
\begin{itemize}
\item no neighbors in the current image (time t)
\item in the previous image (time t-1) and the next image (time t+1) : use C4 neighbors
\end{itemize}

\item CTN (for time series):
\begin{itemize}
\item no neighbors in the current image (time t)
\item in the previous image (time t-1) and the next image (time t+1) : use "C4X" neighbors, that is consider the 4 possibles corners (top-left, top-right, bottom-left and bottom-right corners)
\end{itemize}

\end{itemize}

\vspace{1em}
The connectivity can be combine with the operator `|'. Note this special character can be interpreted by the shell.

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
prg /images/i.tif /images/o.tif -c 0 --showConfig `tty` --connectivity "C4|CT"

prg /images/i.tif /images/o.tif -c 0 --showConfig `tty` --connectivity C4\|CT
\end{lstlisting}

\begin{figure}[htbp]
\centering
\includegraphics[width=4cm]{../images/connectivity.pdf}
\caption{Different pixel connectivities}
\label{fig:connectivity}
\end{figure}
