# read input image, keeps only band 0, save the output image and the config file
prg /images/arles.tif /images/o.tif --saveChannel config --copyBands 0

# read input image, create a new NDVI band from band 0 and 1, then save the output image
# and the config file option copyBand keeps the newly created band (=band 3) in config.
# By default it is not in the config ! If this option is omitted, it will produce this
# message: ``error: No output band selected''
prg /images/arles.tif /images/o.tif --saveChannel config --ndviBands 0,1 --copyBand 3

# read input image, from band 0 create a AP image using Min-Tree and Area attribute with
# thresholds 1000 and 2500, then save the output image and the config file (the 2
# following lines are equivalent, with long then short parameters)
prg /images/arles.tif /images/o.tif --saveChannel config \
		 --withBands 0 --treeType Min --attributeType Area \
		 --thresholds 1000,2500
prg /images/arles.tif /images/o.tif --saveChannel config \
		 -b 0 -t Min -a Area \
		 --thresholds 1000,2500

# read input image, perform the same AP as previously, add a DAP band, then save the
# output image and the config file
prg /images/arles.tif /images/o.tif --saveChannel config -b 0 -t Min -a Area \
		 --thresholds 1000,2500 --dapPos apOrigPosBegin

# read input image, produce Sobel band from band 1, then save the output image and the
# config file option copyBand keeps the newly created band (=band 3) in config.
# By default it is not in the config ! If this option is omitted, it will produce this
# message: ``error: No output band selected''
prg /images/arles.tif /images/o.tif --saveChannel config --sobel 1 --copyBand 3

# read input image, produce Haar band from band 1, then save the output image and the
# config file. By default the band is in the config
prg /images/arles.tif /images/o.tif --saveChannel config -b 0 --haarSizes 5x3

# loads the content of previously created config file (config.ch) then show its content
# Warning: as there is no output image, we must use cfg, not prg
cfg --loadChannel config --showChannel

