# Classification using a min-tree and a ground truth image (``arlesGT.tif'').
prg /images/arles.tif /images/o.tif --saveChannel config \
    -b 0 -t Min -a Area --thresholds 1000,2500 \
    -g /images/arlesGT.tif --fgValue 3

# Classification using a max-tree and a ground truth image.
prg /images/arles.tif /images/o.tif --saveChannel config \
    -b 0 -t Max -a Area --thresholds 1000,2500 \
    -g /images/arlesGT.tif --fgValue 3

# Classification using a median-tree and a ground truth image.
prg /images/arles.tif /images/o.tif --saveChannel config \
    -b 0 -t Med -a Area --thresholds 1000,2500 \
    -g /images/arlesGT.tif --fgValue 3

# Classification using a max-tree, a NDVI band and a ground truth image.
prg /images/arles.tif /images/o.tif --saveChannel config \
    -n 0,1 -c 3 -b 3, -t Max -a Area --thresholds 1000,2500,5000,7500 \
    -g /images/arlesGT.tif, --fgValue 3

