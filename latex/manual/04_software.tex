\section{Software overview}

\subsection{Functional architecture}

Figure \ref{fig:architecture-global} show a global view of the process chain of the software (including the possible classification process made outside Triskele). The next figure \ref{fig:architecture-details} shows a more detailed view of the processes involved.

\begin{figure}[htbp]
\centering
\ifTriskele{
  \includegraphics[width=14cm]{../images/triskele-architecture-global.pdf}
}\ifBroceliande{
  \includegraphics[width=14cm]{../images/broceliande-architecture-global.pdf}
}
\caption{Functional architecture (global view)}
\label{fig:architecture-global}
\end{figure}

\begin{figure}
\centering
\ifTriskele{
  \includegraphics[width=12cm]{../images/triskele-architecture-details.pdf}
}
\ifBroceliande{
  \includegraphics[width=12cm]{../images/broceliande-architecture-details.pdf}
}
\caption{Functional architecture (detailed view)}
\label{fig:architecture-details}
\end{figure}

\subsection{Optimization}

The Obelix team has developed optimized algorithms which are fast and not much memory consuming. Every tree is build with using linear complexity (with the number of pixels), regardless of the type of tree or other parameters. Also the building process can be parallelized, so that the overall computational time will be divided by the number of available processors.

\subsection{Computational steps}

Below we present a global view of the different computational steps that this program will run into, in order to have a general idea of the different processes applied. Almost all steps are optional, except reading the metadata and the content of the input image.

\subsubsection{Reading the configuration file}
The very first step is to read the configuration file (if required). The configuration file can be seen as a set of default options, that might be completed and/or overridden by all the following manual options.

\subsubsection{Parsing options and reading metadata}
So the very next step is to analyze all the different options given by the users, including input and/or output image filenames if provided. Please refer to section \ref{sec:formatting} and following for more details about those options.
A this stage, the program also reads the metadata in the input image, allowing the program to set up for the proper pixel type (integer of floating point, so far complex type like in SAR images is not allowed).

\subsubsection{Cropping the image}
The input image is cropped according to a rectangular selection zone. By default this rectangular zone is the whole image. A partial selection can be extremely useful when the input image is to large to be analyzed in a single go.

\subsubsection{Reading the image}
All the bands of the image are read (which is useful at least to identify the outside of the image, where the pixels have a “no-data” value). We use the GDAL library for this stage, which means that our software can read any image format supported by this library.

\subsubsection{Finding the inside and the outside of the image}
Before reading the image, by default all the pixels are considered as being outside the image, i.e. having a “no-data” values. Then when reading the image, all the pixels that has a non-zero value in any of the bands will be promoted as being inside the image to be analyzed. Otherwise they will still be considered as being
outside the image.

\subsubsection{Computing NDVI}
Whenever reading a image band, we check whether it is part of the NDVI computation. In that case, the program could keep in memory either the content of this band, either the result of NDVI. Note that so far we do not consider computing more than one NDVI band. Please refer to the section \ref{sec:band-production} for more details.

\subsubsection{Computing Sobel band}
Whenever reading a image band, we check whether it is part of the Sobel band computation. In that case the program can run the computation (linear time complexity) on this band. Please refer to section \ref{sec:band-production} for more details.

\subsubsection{Selecting the bands}
For each band selected by the user (and/or the configuration file) the computation for attribute profiles and texture profiles will be done.
For texture profiles, only “integral images” are produced and kept (see section \ref{sec:integral-image}). If required, Haar features or statistics features will be produced on the fly when necessary.
For attribute profiles, they are created and kept. If required, the DAP (Difference of Attribute Profile) will be produced on the fly when necessary.

\subsubsection{(Pre)computing texture profiles}
All texture profiles are produced using linear time complexity algorithms, as their computation is based on “integral images”. Actually, an integral image is defined as being the sum of all the pixel values in the region above and to the left of the current pixel. The computation of the integral image requires only a linear time complexity. Then once computed, it can provide very quickly the sum of pixel values on any rectangular zone of the image in an extremely short and constant time, using only differences in the integral image. For more information on integral images, please refer to section \ref{sec:integral-image}.

\subsubsection{(Pre)computing Attribute Profiles (AP)}
The computation of Attribute profiles (AP) is based on the computation of morphological trees: min-tree, max-tree or median-trees. So for each band, for example a min-tree is created then the attribute profiles are computed. For each AP, trees are pruned according to the list of thresholds given by the user (and/or the configuration file).
Here there is an implementation issue for this software about choosing the best possible memory usage when computing trees and attribute profiles. In other words we want to determine what is best choice between keeping in memory the trees or keeping in memory the attribute profiles. This choice relies on the number and the type of pixels, but also on the number of user thresholds.
To give a typical example, we can consider that below 13 thresholds it is better for memory usage to compute the AP then delete the tree structure, whereas above 13 thresholds it is better to keep only the tree structure. This example is based on an image of 20 000 x 90 000 pixels using 16-bit depth. However, for any different situation, or for example if we consider only a sub-part of the image, the previous criterion should be reconsidered.

\subsection{Configuration files (a.k.a. “channel files”)}
\label{sec:configuration-file}

Configuration files is a useful way to store and retrieve a set of options used to compute the output images. The configuration files are stored in special files called “channel files” (extension *.ch). For readability they are written in XML format. Here is an example of such a channel file:

\begin{lstlisting}[language=XML,breaklines]
<?xml version="1.0" ?>
<Triskele type="UInt16" bandCount="3" created="2019-03-07 17:33:08" modified="2019-03-07 17:33:08">
    <AP id="3" from="0" treeType="Min" featureType="AP" attributeType="Area">
        <Cut threshold="1000" />
        <Cut threshold="2500" />
    </AP>
    <DAP id="4" doWrite="true" from="3" apOrigPos="apOrigPosBegin" />
</Triskele>
\end{lstlisting}
