\section{Options for textures}

\subsection{Note about Integral Images}
\label{sec:integral-image}

For the following options for textures, we will first compute a special image called "integral image" which is a computational optimization allowing us to perform the computation with a linear complexity.

\subsubsection{Definition}
The integral image (a.k.a. "Summed Area Table") $I$ of an original image $i$ is defined such as a point $(x_0,y_0)$ of the integral image is the sum of all pixel values in the original image above and to the left of this point $(x_0,y_0)$, inclusive (see figure \ref{fig:integral-image-definition}). In other words:

\[
I(x_0,y_0)=\sum_{\substack{ 0 \leq x \leq x_0 \\
                            0 \leq y \leq y_0 }}
            i(x,y)
\]

\begin{figure}[htbp]
\centering
\includegraphics[width=6cm]{../images/TextF/integral-image-definition.pdf}
\caption{Definition of an Integral Image}
\label{fig:integral-image-definition}
\end{figure}

\subsubsection{Creation}

Actually the fastest way to create the Integral Image is not by using its definition but instead by using the following property (where i is the original image and I the integral image currently being creating):

\[
I(x_0,y_0) = i(x_0,y_0) + I(x_0,y_0-1) + I(x_0-1,y_0) - I(x_0-1,y_0-1)
\]

This property is interesting as it allows to build the integral image simply and incrementally, in other word each point of the integral image is deduced from 3 other points already computed in the integral image and 1 point from the original image. So that actually we never have to perform a multiple sum.

It is also important to notice that for any term $I(..., ...)$ in the formula, whenever one of the coordinate (or both) is equal to $0$, we should replace the corresponding this term by $0$. This situation is equivalent to adding one extra columns on the left and one extra row at the top of the integral image, both filled with $0$.

In figure \ref{fig:integral-image-creation} we can see a numerical example of such a creation process.
The point with value $33$ in the integral image corresponds to the summed-area of the red zone in original image and is computed as follow:

\[
{\color{red}33} = {\color{green}10} + {\color{blue}14} + {\color{blue}15} - {\color{cyan}6}
\]

\begin{figure}[htbp]
\centering
\includegraphics[width=4cm]{../images/TextF/integral-image-creation.pdf}
\caption{Creation of an Integral Image \\
        (left:original image, right:integral image)}
\label{fig:integral-image-creation}
\end{figure}

\subsubsection{Usage}

An interesting property is that the summed-are of any rectangular region $[x_1;x_2,]\times[y_1;y_2]$, in the original image can be computed using only 4 points of the integral image $I$:

\[
\sum_{\substack{    x_1 \leq x \leq x_2 \\
                    y_1 \leq y \leq y_2 }}
            i(x,y)
=I(x_2,y_2) + I(x_1-1,y_1-1) - I(x_2,y_1-1) - I(x_1-1,y_2)
\]

Actually we can notice than the first property that was used for creation is just a special case of this second property (the rectangular region being only a mere point $(x,y)$ so that $x_1=x_2=x_0$ and $y_1=y_2=y_0$).

In figure \ref{fig:integral-image-usage} we can see an numerical example of usage of the integral matrix:

\begin{figure}[htbp]
\centering
\includegraphics[width=4cm]{../images/TextF/integral-image-usage.pdf}
\caption{Usage of an Integral Image \\
        (left:original image, right:integral image)}
\label{fig:integral-image-usage}
\end{figure}


\begin{itemize}
\item on the original image, in order to compute the summed-area on the red rectangle, we can compute the sum of the whole image, then subtract the sum on the blue rectangle, subtract the sum on the yellow rectangle, then add the sum on the green rectangle (because it was subtracted twice).
\item on the integral image, we can obtain the same result with a much more simple operation using only 4 points (note that each colored point on the integral image corresponds to the bottom-right corner of the colored rectangle in the original image):

\[
72 = {\color{red}136} - {\color{orange}36} - {\color{cyan}28} + {\color{green}6}
\]

\end{itemize}

\subsection{-H arg, {\doubleDash}haarSizes arg}

Compute Haar-like features for a given window size given in argument (in format $H\times W$).
Here 4 different features are produced and put into 4 different bands:

\begin{itemize}
\item $H_x$
\item $H_x+H_y$
\item $H_y$ 
\item $H_y+H_x$
\end{itemize}

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 --haar 20x20 --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c c c c c}
    Hx & & Hx+Hy & & Hy & & Hy-Hx & & Haar (20x20) \\
    \includegraphics[width=2.4cm]{../images/TextF/arles-mini-haar-20-20-x.png} & & 
    \includegraphics[width=2.4cm]{../images/TextF/arles-mini-haar-20-20-xy.png} & & 
    \includegraphics[width=2.4cm]{../images/TextF/arles-mini-haar-20-20-y.png} & & 
    \includegraphics[width=2.4cm]{../images/TextF/arles-mini-haar-20-20-yx.png} & &
    \includegraphics[width=2.4cm]{../images/TextF/arles-mini-haar-20-20.png} \\
    \includegraphics[width=2.4cm]{../images/TextF/arles-crop-haar-20-20-x.png} & & 
    \includegraphics[width=2.4cm]{../images/TextF/arles-crop-haar-20-20-xy.png} & & 
    \includegraphics[width=2.4cm]{../images/TextF/arles-crop-haar-20-20-y.png} & & 
    \includegraphics[width=2.4cm]{../images/TextF/arles-crop-haar-20-20-yx.png} & &
    \includegraphics[width=2.4cm]{../images/TextF/arles-crop-haar-20-20.png} \\
  \end{tabular}
\end{center}

\subsubsection{Computation}

The Haar-like features are named upon the Haar wavelets as they follow the same principle. Historically they were use for face recognition and more generally for object recognition. They are usually computed in a moving rectangular A windows, which is itself divided into several regions in order to detect a precise pattern. Here we will only use rectangular features which means each window is divided into 2 rectangular regions, in order to detect horizontal, vertical or diagonal patterns.

For example as we can see in figure \ref{fig:haar-vert-hor} the $H_x$ feature is meant to detect vertical patterns (edges of change in texture for example). It gives a non-zero value each time we have a difference between the 2 vertical sub-rectangles. In the same way the $H_y$ feature is meant to detect horizontal patterns. The diagonal patterns are discussed below.

\begin{figure}[htbp]
\centering
\includegraphics[width=5cm]{../images/TextF/haar-vert-hor.pdf}
\caption{Vertical and horizontal Haar features}
\label{fig:haar-vert-hor}
\end{figure}

\subsubsection{Optimization (vertical and horizontal features)}

As we said previously, the Haar features are computed from integral images. The summed area is computed on each sub-rectangle of the moving window by using 4 points in the integral image for each sub-rectangle. For example to compute $H_x$ or $H_y$ we will need to compute the difference of 2 summed-area on surfaces $A$ and $B$, which will be done by using the values of points $a,b,c,d,e,f$ in the integral images, as follow (see figure \ref{fig:haar-vert-hor}):

\[
B-A = (f+b-e-c)-(e+a-b-d) = f+2b+d-a-c-2e
\]

\begin{figure}[htbp]
\centering
\includegraphics[width=5cm]{../images/TextF/haar-optim-vert-hor.pdf}
\caption{Vertical and horizontal Haar features: optimization using integral image}
\label{fig:haar-optim-vert-hor}
\end{figure}

\subsubsection{Optimization (diagonal features)}

Theoretically, the diagonal features $H_a$ and $H_b$ with directions $\pi/4$ and $3\pi/4$ computed from should be $H_x$ and $H_y$ as:

\[
H_a =
\begin{pmatrix}
    \cos(\pi/4).H_x \\
    \sin(\pi/4).H_y \\
\end{pmatrix}
\]
\[
H_b =
\begin{pmatrix}
    -\sin(\pi/4).H_x \\
    \cos(\pi/4).H_y \\
\end{pmatrix}
\]

Actually we can avoid the cost of computing the trigonometric functions, because usually we are only interested by the direction of those patterns.
One of the reason is that we want to use those features for future classification and we will mainly focus on a ``random forest'' type of classifier, which is indifferent to sign and scale. So in that case, as we can see in figure \ref{fig:haar-optim-diagonal}, the following features will give the same results:

\[
H_{x+y} =
\begin{pmatrix}
    H_x \\
    H_y \\
\end{pmatrix}
\]
\[
H_{x-y} =
\begin{pmatrix}
    -H_x \\
    H_y \\
\end{pmatrix}
\]

\begin{figure}[htbp]
\centering
\includegraphics[width=5cm]{../images/TextF/haar-optim-diagonal.pdf}
\caption{Diagonal Haar features: optimization using horizontal and vertical features}
\label{fig:haar-optim-diagonal}
\end{figure}

\subsubsection{Complexity}

The Haar-like features will be based on the computation of an integral image. In a similar way, the statistics features will be based on the computation of a square integral image. A square integral image is simply the integral image of the square values of the original image.

The complexity of creation for an integral image is linear (with the number of pixels), while the complexity of usage is constant. The same result apply for the square integral image that implies only a additional step with linear complexity (computing the square value of all pixels).
So the overall complexity of the integral image (or square integral image) is always linear.

\subsection{-T arg, {\doubleDash}statSizes arg}

Compute statistics features for a given window size given in argument (in format $H\times W$).
Currently the statistics produce the following bands:

\begin{itemize}
\item Mean
\item Standard-deviation
\item Entropy
\end{itemize}

The following listing use the \emph{prg} alias (i.e. \prgName).
\begin{lstlisting}[language=bash, breaklines=true, 
    postbreak=\mbox{\textcolor{red}{$\hookrightarrow$}\space}, style=colorCommandStyle]
  prg /images/arlesGS.tif /images/o.tif -b 0 --stat 10x20 --time
\end{lstlisting}

\begin{center}
  \begin{tabular}{c c c c c c c}
    Mean & ~ & SD & ~ & Ent & ~ & Stat (10x20)\\
    \includegraphics[width=3cm]{../images/TextF/arles-mini-stat-20-20-mean.png} & &
    \includegraphics[width=3cm]{../images/TextF/arles-mini-stat-20-20-sd.png} & &
    \includegraphics[width=3cm]{../images/TextF/arles-mini-stat-20-20-ent.png} & &
    \includegraphics[width=3cm]{../images/TextF/arles-mini-stat-20-20.png} \\
    \includegraphics[width=3cm]{../images/TextF/arles-crop-stat-20-20-mean.png} & &
    \includegraphics[width=3cm]{../images/TextF/arles-crop-stat-20-20-sd.png} & &
    \includegraphics[width=3cm]{../images/TextF/arles-crop-stat-20-20-ent.png} & &
    \includegraphics[width=3cm]{../images/TextF/arles-crop-stat-20-20.png} \\
  \end{tabular}
\end{center}

\subsubsection{Computation}

We consider a window containing $n$ pixels named $p_i$ (with $0\leq i\leq n-1$)

Then the mean value is computed as:

% "\limits" allows to display properly the limits of the sum
\[
mean = \frac{\sum\limits_{i=0}^{n-1} p_i}{n}
\]

The variance and standard-deviation are computed as:

\[
var = \frac{\sum\limits_{i=0}^{n-1} p_i^2}{n} - mean^2
\]
\[
std = \sqrt{var}
\]

\subsubsection{Complexity}

We remind the in all the previous formula, the sums are not performed directly, but instead taken either from the integral image (for $\sum p_i$) or from the square integral image (for $\sum p_i^2$).

For that reason, the complexity of all the statistics features remain linear (with the number of pixels).

The figure \ref{tab:textTime} indicates time of computation for texture. It was launch on AMD Ryzen Threadripper 1950X (2*16 cores). That don't take in account build tree depending the type of it.

\begin{figure}
  \centering
  \begin{tabular}{c c c c}
    & integral image & square integral image & "the function" \\
    \hline
    Haar & 0.0498 s &  & 0.1126 s \\
    Stat & 0.0374 s & 0.0475 s & 0.0923 s \\
  \end{tabular}
  \caption{computation time according texture}
  \label{tab:textTime}
\end{figure}

\subsection{{\doubleDash}noHaar}

Do not keep the Haar feature band in in list of bands to produce

\subsection{{\doubleDash}noStat}

Do not keep the statistics feature band in the list of bands to produce
