#!/bin/bash

# Pull TRISKELE docker image

docker pull registry.gitlab.inria.fr/obelix/triskele/master:latest
docker images registry.gitlab.inria.fr/obelix/triskele/master | grep triskele
docker run -it registry.gitlab.inria.fr/obelix/triskele/master
