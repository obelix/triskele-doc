#!/bin/bash

# Pull Broceliande docker image

docker pull registry.gitlab.inria.fr/obelix/broceliande/master:latest
docker images registry.gitlab.inria.fr/obelix/broceliande/master | grep broceliande
docker run -it registry.gitlab.inria.fr/obelix/broceliande/master
